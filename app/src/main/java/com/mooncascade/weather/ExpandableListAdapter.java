package com.mooncascade.weather;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by kairi on 6.07.2015.
 */
class ExpandableListAdapter extends BaseExpandableListAdapter {

    private final Resources resources;
    private final LayoutInflater inflater;

    private final List<Forecast> forecasts;

    private final String[] numbers;
    private final String[] tens;

    public ExpandableListAdapter(Activity context, List<Forecast> forecasts) {
        this.forecasts = forecasts;

        resources = context.getResources();
        inflater = context.getLayoutInflater();

        numbers = resources.getStringArray(R.array.numbers_array);
        tens = resources.getStringArray(R.array.tens_array);
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String date = forecasts.get(groupPosition).date;
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.group_item, null);
        }
        TextView item = (TextView) convertView.findViewById(R.id.dateTextView);
        item.setTypeface(null, Typeface.BOLD);
        item.setText(date);
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final Forecast forecast = forecasts.get(groupPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.child_item, null);
        }

        TextView dayTempView = (TextView) convertView.findViewById(R.id.dayTemperaturetextView);
        TextView dayTempTextView = (TextView) convertView.findViewById(R.id.dayTempTextView);
        TextView dayDescriptionView = (TextView) convertView.findViewById(R.id.dayDescriptiontextView);
        TextView dayWindSpeedView = (TextView) convertView.findViewById(R.id.dayWindSpeedtextView);

        TextView nightTempView = (TextView) convertView.findViewById(R.id.nightTemperaturetextView);
        TextView nightTempTextView = (TextView) convertView.findViewById(R.id.nightTempTextView);
        TextView nightDescriptionView = (TextView) convertView.findViewById(R.id.nightDescriptiontextView);
        TextView nightWindSpeedView = (TextView) convertView.findViewById(R.id.nightWindSpeedtextView);

        dayTempView.setText(resources.getString(R.string.temperature_range, forecast.dayData.minTemp, forecast.dayData.maxTemp));
        dayDescriptionView.setText(forecast.dayData.description);
        dayTempTextView.setText(getTemperatureRangeAsText(forecast.dayData));

        nightTempView.setText(resources.getString(R.string.temperature_range, forecast.nightData.minTemp, forecast.nightData.maxTemp));
        nightDescriptionView.setText(forecast.nightData.description);
        nightTempTextView.setText(getTemperatureRangeAsText(forecast.nightData));

        if(groupPosition == 0) {
            dayWindSpeedView.setText(resources.getString(R.string.wind_range, forecast.dayData.minWind, forecast.dayData.maxWind));
            dayWindSpeedView.setVisibility(View.VISIBLE);

            nightWindSpeedView.setText(resources.getString(R.string.wind_range, forecast.nightData.minWind, forecast.nightData.maxWind));
            nightWindSpeedView.setVisibility(View.VISIBLE);
        } else {
            dayWindSpeedView.setVisibility(View.GONE);
            nightWindSpeedView.setVisibility(View.GONE);
        }

        return convertView;
    }

    private String getTemperatureRangeAsText(Forecast.WeatherData dayData) {
        String minTempText = convertToText(dayData.minTemp);
        String maxTempText = convertToText(dayData.maxTemp);

        return String.format(resources.getString(R.string.temperature_range_text), minTempText, maxTempText);
    }

    private String convertToText(int number) {
        StringBuilder result = new StringBuilder(number < 0 ? resources.getString(R.string.minus): "");
        number = Math.abs(number);

        if(number < 20) {
            result.append(numbers[number]);
        } else {
            result.append(tens[number / 10]).append(" ").append(numbers[number % 10]);
        }
        return result.toString();
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public int getGroupCount() {
        return forecasts.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return forecasts.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return forecasts.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
