package com.mooncascade.weather;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.util.Xml;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class MainActivity extends Activity {

    private LoadFeedTask feedTask = null;

    private ProgressBar mProgressView;

    private NetworkReceiver receiver;

    private boolean isConnected;
    private View connectionStatusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressView = (ProgressBar) findViewById(R.id.login_progress);
        connectionStatusTextView = findViewById(R.id.waitingForConnectionTextView);

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        receiver = new NetworkReceiver();
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        receiver.updateConnectionStatus(this);
        loadWeatherData();
    }

    private void loadWeatherData() {
        showProgress(true);
        if(!isConnected) {
            Toast.makeText(this, R.string.error_no_connection, Toast.LENGTH_LONG).show();
            showProgress(false);
            return;
        }
        feedTask = new LoadFeedTask();
        feedTask.execute();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    public class LoadFeedTask extends AsyncTask<Void, Void, List> {

        private final String WEATHER_FEED_URL = "http://www.ilmateenistus.ee/ilma_andmed/xml/forecast.php";

        @Override
        protected List doInBackground(Void... params) {
            try {
                URL url = new URL(WEATHER_FEED_URL);
                XmlPullParser parser = Xml.newPullParser();
                parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
                parser.setInput(url.openConnection().getInputStream(), "ISO-8859-1");
                return ForecastFeedParser.readFeed(parser);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final List result) {
            feedTask = null;
            if(result == null) {
                Toast.makeText(MainActivity.this, R.string.error_download_failure, Toast.LENGTH_LONG).show();
            } else {
                ExpandableListView listView = (ExpandableListView) findViewById(R.id.forecastList);
                final ExpandableListAdapter listAdapter = new ExpandableListAdapter(MainActivity.this, result);
                listView.setAdapter(listAdapter);
                listView.expandGroup(0);
            }
            showProgress(false);
        }

        @Override
        protected void onCancelled() {
            feedTask = null;
            showProgress(false);
        }
    }

    public class NetworkReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            boolean oldConnectionStatus = isConnected;
            
            updateConnectionStatus(context);

            if (!oldConnectionStatus && isConnected) {
                // network connection reestablished, reload feed
                loadWeatherData();
            }
        }

        private void updateConnectionStatus(Context context) {
            ConnectivityManager connMgr =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            isConnected = networkInfo != null && networkInfo.isConnected();
            updateConnectionStatusView(isConnected);
        }

        private void updateConnectionStatusView(boolean isConnected) {
            connectionStatusTextView.setVisibility(isConnected ? View.GONE : View.VISIBLE);
        }
    }
}

