package com.mooncascade.weather;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kairi on 6.07.2015.
 */
class ForecastFeedParser {

    private static final String ns = null;

    private static final String TAG_FORECASTS   = "forecasts";
    private static final String TAG_FORECAST    = "forecast";
    private static final String TAG_NIGHT       = "night";
    private static final String TAG_DAY         = "day";
    private static final String TAG_TEMPMIN     = "tempmin";
    private static final String TAG_TEMPMAX     = "tempmax";
    private static final String TAG_WIND        = "wind";
    private static final String TAG_TEXT        = "text";
    private static final String TAG_SPEEDMIN    = "speedmin";
    private static final String TAG_SPEEDMAX    = "speedmax";

    static List readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Forecast> forecasts = new ArrayList();
        parser.nextTag();
        parser.require(XmlPullParser.START_TAG, ns, TAG_FORECASTS);
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(TAG_FORECAST)) {
                forecasts.add(readForecast(parser));
            } else {
                skip(parser);
            }
        }
        return forecasts;
    }

    private static Forecast readForecast(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, TAG_FORECAST);
        Forecast forecast = new Forecast();
        forecast.date = readDate(parser);

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case TAG_DAY:
                    forecast.dayData = readWeatherData(parser);
                    break;
                case TAG_NIGHT:
                    forecast.nightData = readWeatherData(parser);
                    break;
                default:
                    skip(parser);
            }
        }
        return forecast;
    }

    private static Forecast.WeatherData readWeatherData(XmlPullParser parser) throws IOException, XmlPullParserException {
        Forecast.WeatherData weatherData = new Forecast.WeatherData();
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case TAG_TEMPMIN:
                    weatherData.minTemp = readInt(parser, TAG_TEMPMIN);
                    break;
                case TAG_TEMPMAX:
                    weatherData.maxTemp = readInt(parser, TAG_TEMPMAX);
                    break;
                case TAG_WIND:
                    readWind(weatherData, parser);
                    break;
                case TAG_TEXT:
                    weatherData.description = readText(parser, TAG_TEXT);
                    break;
                default:
                    skip(parser);
            }
        }
        return weatherData;
    }

    private static void readWind(Forecast.WeatherData weatherData, XmlPullParser parser) throws IOException, XmlPullParserException {
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case TAG_SPEEDMIN:
                    weatherData.minWind = Math.min(weatherData.minWind, readInt(parser, TAG_SPEEDMIN));
                    break;
                case TAG_SPEEDMAX:
                    weatherData.maxWind = Math.max(weatherData.minWind, readInt(parser, TAG_SPEEDMAX));
                    break;
                default:
                    skip(parser);
            }
        }
    }

    private static int readInt(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        int value = readInt(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return value;
    }

    private static String readText(XmlPullParser parser, String tag) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, tag);
        String value = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, tag);
        return value;
    }

    private static String readDate(XmlPullParser parser) {
        return parser.getAttributeValue(0);
    }

    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private static int readInt(XmlPullParser parser) throws IOException, XmlPullParserException {
        int result = 0;
        if (parser.next() == XmlPullParser.TEXT) {
            result = Integer.parseInt(parser.getText());
            parser.nextTag();
        }
        return result;
    }

    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}