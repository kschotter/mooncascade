package com.mooncascade.weather;

/**
 * Created by kairi on 3.07.2015.
 */
public class Forecast {

    public WeatherData nightData;
    public WeatherData dayData;
    public String date;

    static class WeatherData {

        public int minTemp;
        public int maxTemp;

        public int minWind = Integer.MAX_VALUE;
        public int maxWind = Integer.MIN_VALUE;

        public String description;
    }
}
